package com.china.region

import org.mybatis.spring.annotation.MapperScan
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableAsync

@SpringBootApplication(scanBasePackages = ["com.china.region", "com.houkunlin.system.common"])
@EnableAsync
@MapperScan(value = ["com.china.region.repository"])
class Application

fun main(args: Array<String>) {
    runApplication<Application>(*args)
}
