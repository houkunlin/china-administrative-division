package com.china.region.entity

import com.baomidou.mybatisplus.annotation.*
import com.china.region.enums.LevelType
import com.china.region.enums.RegionType
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * 行政区划代码
 *
 * @author HouKunLin
 */
@TableName("region_2023_simple")
data class RegionEntity(
    /**
     * 租户ID
     */
    var tenantId: Long = 0,
    /**
     * 乐观锁字段
     */
    @field:[Version]
    var revision: Int = 0,
    /**
     * 逻辑删除
     */
    @field:[TableField("is_deleted") TableLogic(value = "false", delval = "true")]
    var deleted: Boolean = false,
    /**
     * 创建人
     */
    var createdBy: Long? = null,
    /**
     * 创建时间
     */
    @field:[TableField(updateStrategy = FieldStrategy.NEVER)]
    var createdTime: LocalDateTime = LocalDateTime.now(),
    /**
     * 修改人
     */
    var updatedBy: Long? = null,
    /**
     * 修改时间
     */
    var updatedTime: LocalDateTime = LocalDateTime.now(),
    /**
     * 主键ID
     */
    @field:[TableId(type = IdType.ASSIGN_ID)]
    var id: Long? = null,
    /**
     * 省、直辖市、自治区代码
     */
    var provinceCode: String = "",
    /**
     * 城市代码
     */
    var cityCode: String = "",
    /**
     * 区县代码
     */
    var countyCode: String = "",
    /**
     * 乡镇代码
     */
    var townCode: String = "",
    /**
     * 街道代码
     */
    var streetCode: String = "",
    /**
     * 街道名称
     */
    var streetName: String = "",
    /**
     * 城乡街道分类代码
     */
    var streetType: String = "",
    /**
     * 数据级别：省市区县乡镇街道
     */
    @field:[EnumValue]
    var levelType: LevelType? = null,
    /**
     * 区划激活时间
     */
    var enabledDate: LocalDate = LocalDate.now(),
    /**
     * 第一次出现的年份
     */
    var createdDate: LocalDate = LocalDate.now(),
    /**
     * 某个区域被取消的时候会在这里标记年份
     */
    var disabledDate: LocalDate? = null,
    /**
     * 取消的原因
     */
    var disabledMark: String? = null,
    /**
     * 变更日志
     */
    var log: String = "",
    /**
     * 由于历史变更，有些乡变镇，县变区，县变市的城市会从历史中消失不见，此时将该记录给隐藏
     */
    @field:[TableField("is_enabled")]
    var enabled: Boolean = true,
    /**
     * 信息验证时间，最新检查时间
     */
    var verifyDate: LocalDateTime = LocalDateTime.now(),
    /**
     * 数据类型
     */
    @field:[EnumValue]
    var regionType: RegionType? = null,
    /**
     * 是否处理完子类的信息
     */
    var childFinish: Boolean = false,
    /**
     * 备注
     */
    var remark: String? = null,
)
