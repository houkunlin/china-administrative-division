package com.china.region.gb10114

import com.china.region.entity.RegionEntity
import com.china.region.enums.LevelType
import com.china.region.enums.RegionType
import com.china.region.service.RegionService
import com.china.region.util.toLocalDate
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import java.io.File
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import kotlin.math.pow

/**
 * 处理国家统计局的行政区划代码网页文件
 *
 * @property regionService 行政区划保存数据的Service文件
 * @constructor Create empty Stats html handler
 */
@Component
class StatsHtmlHandler(val regionService: RegionService?) : CommandLineRunner {
    /**
     * 正则：年份标题1
     */
    val beforeYearTitle = Regex(".*截止([0-9]{4})年([0-9]{1,2})月([0-9]{1,2})日.*")

    /**
     * 正则：年份标题2
     */
    val beforeYearTitle1 = Regex(".*([0-9]{4})年([0-9]{1,2})月([0-9]{1,2})日.*")

    /**
     * Count : 2019 - 5438307
     */
    var count = 0

    companion object {
        private val logger = LoggerFactory.getLogger(StatsHtmlHandler::class.java)!!

        @JvmStatic
        fun main(args: Array<String>) {
            StatsHtmlHandler(null).run()
        }
    }

    /**
     * Run
     *
     * @param args
     */
    override fun run(vararg args: String?) {
        val start = System.nanoTime()
        val parent = File("data/GB10114-88/2023")
        parseFile(parent, null) { dateTime, file ->
            if (logger.isDebugEnabled) {
                logger.debug("正在解析 {} , file: {}", dateTime, file)
            }
            val filename = file.nameWithoutExtension
            if (filename.contains("index")) {
                parseRegion(dateTime!!, file)
            } else {
                parseMore(dateTime!!, file)
            }
        }
        regionService?.setAllValidDate(LocalDateTime.now(), RegionType.GB10114_88)
        val end = System.nanoTime()
        logger.info("已完成，耗时：{}ms", (end - start) / 1000000.0)
        logger.info("总共 {} 条记录", count)
    }

    /**
     * 解析一个文件
     *
     * @param file 网页文件
     * @param date 年份日期
     * @param action 操作方法
     * @receiver
     */
    fun parseFile(file: File, date: LocalDate?, action: (LocalDate?, File) -> List<RegionEntity>) {
        if (!file.exists()) {
            return
        }
        if (file.isFile) {
            waitRun {
                val result = action(date, file)
                finish(file, date, result)
                val filename = file.nameWithoutExtension
                parseFile(File(file.parentFile, filename.substring(filename.length - 2)), date, action)
            }
            return
        }
        val files = file.listFiles() ?: emptyArray()
        val dirs = files.filter { it.isDirectory }
        val htmlFiles = files.filter {
            it.isFile && it.extension.lowercase(Locale.getDefault())
                .startsWith("htm") && !it.nameWithoutExtension.lowercase(Locale.getDefault())
                .startsWith("index")
        }
        val indexHtmlFiles = files.filter {
            it.isFile && it.extension.lowercase(Locale.getDefault())
                .startsWith("htm") && it.nameWithoutExtension.lowercase(Locale.getDefault())
                .startsWith("index")
        }
        var dateObject = date
        if (indexHtmlFiles.isNotEmpty()) {
            val index = indexHtmlFiles[0]
            val dateString = getTitle(index.parentFile.name + "-12-31", Jsoup.parse(index.readText()))
            dateObject = dateString.toLocalDate()
            parseFile(index, dateObject, action)
        }
        htmlFiles.forEach {
            parseFile(it, dateObject, action)
        }
        dirs.forEach {
            parseFile(it, dateObject, action)
        }
    }

    /**
     * 完成一个文件的处理
     *
     * @param file 网页文件
     * @param dateTime 年份信息
     * @param list 这个文件解析出来的行政区划对象列表
     */
    fun finish(file: File, dateTime: LocalDate?, list: List<RegionEntity>) {
        val isNew = list.filter { it.townCode.isNotBlank() }
        count += isNew.size
        logger.info("完成了 {} {} 文件的 {} 条记录，当前总共有 {} 条新记录", dateTime, file, list.size, count)
        regionService?.finish(list)
    }

    /**
     * 出现异常时重复处理出现异常的代码块，直到运行成功
     *
     * @param action 执行方法
     * @receiver
     */
    fun waitRun(action: () -> Unit) {
        var num = 1
        while (true) {
            try {
                action()
                break
            } catch (e: Exception) {
                e.printStackTrace()
                val time = if (++num > 10) {
                    ((2.0.pow(10.0) + num) * 200).toLong()
                } else {
                    (2.0.pow(num * 1.0) * 200).toLong()
                }
                println(String.format("正在睡眠 %s 次 %ss，然后进行重试", num, time))
                sleep(time)
            }
        }
    }

    /**
     * Sleep
     *
     * @param time
     */
    fun sleep(time: Long) {
        try {
            Thread.sleep(time)
        } catch (e: Exception) {
        }
    }

    /**
     * Parse region
     *
     * @param dateTime
     * @param file
     * @return
     */
    fun parseRegion(dateTime: LocalDate, file: File): List<RegionEntity> {
        val html = file.readText()
        val jsoup = Jsoup.parse(html)
        val trs = jsoup.select("table tr.provincetr a")
        return trs.map { element ->
            val text = element.text()

            val code = File(element.attr("href")).nameWithoutExtension.padEnd(12, '0')

            val entity = RegionEntity(
                streetCode = code,
                streetType = "",
                streetName = text,
                enabledDate = dateTime,
                createdDate = dateTime,
                regionType = RegionType.GB10114_88
            )
            val find = regionService?.findByStreetCode(entity.streetCode)
            if (find == null) {
                regionService?.save(entity)
                entity
            } else {
                find
            }
        }
    }

    /**
     * 获取年份信息
     *
     * @param defaultTitle 默认的名称
     * @param jsoup 网页对象
     * @return
     */
    fun getTitle(defaultTitle: String, jsoup: Document): String {
        val title = jsoup.selectFirst(".provincehead")?.text() ?: ""
        val body = jsoup.selectFirst("body")?.text() ?: ""
        if (logger.isDebugEnabled) {
            logger.debug(title)
        }
        return if (title.contains(beforeYearTitle)) {
            val arr = arrayListOf<String>()
            arr.addAll(beforeYearTitle.matchEntire(title)?.groupValues ?: arrayListOf())
            println(arr)
            arr.removeAt(0)
            if (arr.size > 1 && arr[1].length == 1) {
                arr[1] = "0" + arr[1]
            }
            if (arr.size > 2 && arr[2].length == 1) {
                arr[2] = "0" + arr[2]
            }
            arr.joinToString("-")
        } else if (body.contains(beforeYearTitle1)) {
            val arr = arrayListOf<String>()
            arr.addAll(beforeYearTitle1.matchEntire(body)?.groupValues ?: arrayListOf())
            println(arr)
            arr.removeAt(0)
            if (arr.size > 1 && arr[1].length == 1) {
                arr[1] = "0" + arr[1]
            }
            if (arr.size > 2 && arr[2].length == 1) {
                arr[2] = "0" + arr[2]
            }
            arr.joinToString("-")
        } else {
            defaultTitle
        }
    }

    /**
     * 解析市级、区县级、乡镇级、街道级、居委会级
     *
     * @param dateTime 所属年份
     * @param file 网页文件
     */
    fun parseMore(dateTime: LocalDate, file: File): List<RegionEntity> {
        val html = file.readText()
        val jsoup = Jsoup.parse(html)
        val trs = arrayListOf<Element>()
        jsoup.select(".citytr").forEach { trs.add(it) }
        jsoup.select(".countytr").forEach { trs.add(it) }
        jsoup.select(".towntr").forEach { trs.add(it) }
        jsoup.select(".villagetr").forEach { trs.add(it) }

        if (logger.isInfoEnabled) {
            logger.info("已经得到 {} 的数据 总共 {} 条记录", file, trs.size)
        }

        val result = arrayListOf<RegionEntity>()

        trs.forEach {
            val text = it.text()?.replace(" ", "") ?: ""
            val entity = if (text.isNotBlank()) {
                // 街道（居委会）
                val eq1 = text.contains(Regex("^[0-9]{15}"))
                // 乡镇
                val eq2 = text.contains(Regex("^[0-9]{12}"))
                if (eq1 || eq2) {
                    RegionEntity(
                        // 省级代码
                        provinceCode = text.substring(0, 2),
                        // 市级代码
                        cityCode = text.substring(0, 4),
                        // 县级代码
                        countyCode = text.substring(0, 6),
                        // 乡镇代码
                        townCode = text.substring(0, 9),
                        // 街道代码
                        streetCode = text.substring(0, 12),
                        // 街道类型
                        streetType = if (eq1) text.substring(12, 15) else "",
                        // 街道名称
                        streetName = if (eq1) text.substring(15) else text.substring(12),
                        // 数据级别：省市区乡镇街道
                        levelType = LevelType.getByStreetCode(text.substring(0, 12)),
                        // 是否启用
                        enabled = true,
                        // 激活日期
                        enabledDate = dateTime,
                        // 首次出现日期
                        createdDate = dateTime,
                        // 区域类型
                        regionType = RegionType.GB10114_88
                    )
                } else {
                    if (logger.isInfoEnabled) {
                        logger.info("没有匹配到数据：{}", it.html())
                    }
                    null
                }
            } else {
                null
            }
            entity?.run {
                val find = regionService?.findByStreetCode(this.streetCode)
                if (find == null) {
                    regionService?.save(this)
                    result.add(this)
                } else {
                    result.add(find)
                }
            }
        }
        return result
    }
}
