package com.china.region.util

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

const val DATE_PATTERN = "yyyy-MM-dd"
const val DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss"


fun String.toLocalDate(): LocalDate {
    return LocalDate.parse(this, DateTimeFormatter.ofPattern(DATE_PATTERN))
}

fun String.toLocalDate(pattern: String = DATE_PATTERN): LocalDate {
    return LocalDate.parse(this, DateTimeFormatter.ofPattern(pattern))
}

fun String.toLocalDate(formatter: DateTimeFormatter = DateTimeFormatter.ofPattern(DATE_PATTERN)): LocalDate {
    return LocalDate.parse(this, formatter)
}

fun String.toLocalDateTime(): LocalDateTime {
    return LocalDateTime.parse(this, DateTimeFormatter.ofPattern(DATE_TIME_PATTERN))
}

fun String.toLocalDateTime(pattern: String = DATE_TIME_PATTERN): LocalDateTime {
    return LocalDateTime.parse(this, DateTimeFormatter.ofPattern(pattern))
}

fun String.toLocalDateTime(formatter: DateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN)): LocalDateTime {
    return LocalDateTime.parse(this, formatter)
}
