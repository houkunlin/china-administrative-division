package com.china.region.gb2260

import com.china.region.entity.RegionEntity
import com.china.region.enums.LevelType
import com.china.region.enums.RegionType
import com.china.region.service.RegionService
import com.china.region.util.toLocalDate
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import java.io.File
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * 中华人民共和国行政区划代码
 * http://www.mca.gov.cn/article/sj/fgzd/200707/200707150010089.shtml
 * 从本地的行政区划代码JSON文件中提取出数据保存到数据库中
 *
 * @author HouKunLin
 * @date 2020/8/29 0029 21:22
 */
@Order(101)
@Component
class MacJsonHandler(val regionService: RegionService) : CommandLineRunner {
    private val logger = LoggerFactory.getLogger(MacJsonHandler::class.java)!!

    @Autowired
    lateinit var objectMapper: ObjectMapper

    override fun run(vararg args: String?) {
        val start = System.nanoTime()
        val parent = File("data/GB2260/")
        parent.listFiles()?.forEach file@{ file ->
            logger.debug("处理文件： {}", file)
            val regions = parseJsonFile(file)
            // 全部的区划代码。使用 Set 防止重复，在2019年9月份的数据中，1982年存在重复的区划代码，总共3141条记录，因为11条区划代码重复，实际上只有3130条区划记录，但是在2020年9月份的数据中重复数据被官方修复
            val allCountyCodes = regions.map { it.countyCode }.toSet()
            val createDate = regions[0].createdDate

            if (isIgnore(allCountyCodes, createDate)) {
                logger.debug("忽略{}版本的信息，因为数据库中存在这个版本的所有信息", createDate)
                return@file
            }
            // 处理旧的行政区划代码信息，就是不在当前年份的行政区划代码都设置为不激活状态
            disabled(allCountyCodes, createDate)
            reEnabled(allCountyCodes, createDate)
            // 处理当前年份的行政区划版本
            regions.forEach { entity ->
                val find = regionService.findByCountyCode(entity.countyCode)
                if (find == null) {
                    // 保存新的区划信息
                    regionService.save(entity)
                    // 处理撤县设区，撤县设市
                    val find1 = regionService.findByProvinceCodeAndCountyNameContainingAndIsEnabled(
                        entity.provinceCode,
                        entity.streetName.replace(Regex("(县|市|地区|区)$"), ""),
                        false
                    )
                    if (find1.isNotEmpty()) {
                        println()
                        logger.debug("区划升级-最新数据：{}", objectMapper.writeValueAsString(entity))
                        if (find1.size > 1) {
                            logger.warn(
                                "理论上区划代码变更、区划名称变更、区划等级变更都应该只有一条相应的记录，但是现在找到 {} 条记录，因此需要特别注意看看是否存在一些其他的问题。{} <-- {}",
                                find1.size,
                                "${entity.streetName}(${entity.countyCode})",
                                find1.map { "${it.streetName}(${it.countyCode})" })
                        }
                        find1.forEach che@{ findEntity ->
                            if (findEntity.countyCode == entity.countyCode && findEntity.streetName == entity.streetName) {
//                                logger.debug("相同的信息，不是信息变更1：{}", entity)
//                                logger.debug("相同的信息，不是信息变更2：{}", findEntity)
                                return@che
                            }
                            if (findEntity.disabledMark?.contains("疑似被重新分配") == true) {
                                // 信息已在预处理中禁用，进一步检查具体的禁用细节信息
                                val mark = when {
                                    findEntity.countyCode != entity.countyCode && findEntity.streetName != entity.streetName -> "${entity.enabledDate}行政区划信息变更，具体由[${findEntity.streetName}(${findEntity.countyCode})]变[${entity.streetName}(${entity.countyCode})]"
                                    // 区划变更
                                    findEntity.countyCode != entity.countyCode -> "${entity.enabledDate}行政区划代码重新分配，区划代码由[${findEntity.streetName}(${findEntity.countyCode})]变[${entity.streetName}(${entity.countyCode})]"
                                    // 区划更名
                                    findEntity.streetName != entity.streetName -> "${entity.enabledDate}行政区划名称变更，区划名称由[${findEntity.streetName}(${findEntity.countyCode})]变[${entity.streetName}(${entity.countyCode})]"
                                    // 区划等级变更
                                    else -> "${entity.enabledDate}行政区划等级变更，撤[${findEntity.streetName}(${findEntity.countyCode})]设[${entity.streetName}(${entity.countyCode})]"
                                }
                                findEntity.enabled = false
                                findEntity.disabledDate = createDate
                                findEntity.disabledMark = mark
                                findEntity.log += "reDisabled: $mark\n"
                                regionService.saveOrUpdate(findEntity)
                                logger.debug(
                                    "区划升级-旧数据：{} {} {}",
                                    findEntity.countyCode,
                                    findEntity.streetName,
                                    mark
                                )
                            }
                        }
                    }
                } else {
                    find.run {
                        // 区划名称变更
                        if (streetName != entity.streetName) {
                            val marks =
                                "${entity.enabledDate}行政区划名称变更，区划名称由[$streetName]变[${entity.streetName}]，也有可能是区划代码重新分配一个另一个地区（区划代码互相调换）"
                            streetName = entity.streetName
                            log += "update: $marks\n"
                            regionService.saveOrUpdate(this)
                        }
                    }
                }
            }
        }
        // 更新验证时间
        regionService.setAllValidDate(LocalDateTime.now())
        val end = System.nanoTime()
        logger.debug("已完成，耗时：{}ms", (end - start) / 1000000.0)
    }

    /**
     * 判断是否忽略
     */
    fun isIgnore(allCountyCodes: Set<String>, createDate: LocalDate): Boolean {
        val sqlTheTimeCount = regionService.countByCountyCodeInAndValidDateGreaterThanEqual(allCountyCodes, createDate)
        return sqlTheTimeCount >= allCountyCodes.size
    }

    /**
     * 禁用数据库中已经激活的，但是当前版本中不存在的信息。
     */
    fun disabled(allCountyCodes: Set<String>, createDate: LocalDate) {
        val enabledRegions = regionService.findByCountyCodeNotInAndIsEnabled(allCountyCodes, true)
        logger.debug("在{}被重新规划的信息有{}条", createDate, enabledRegions.size)
        val defaultDisabledMark =
            "该行政区划代码不存在${createDate}版本，疑似被重新分配，如果在以后的还能看到这个提示信息，那就是这个区划代码或者地区被删除，或者改名"
        enabledRegions.forEach {
            it.enabled = false
            it.disabledDate = createDate
            it.disabledMark = defaultDisabledMark
            it.log += "preDisabled: $defaultDisabledMark\n"
        }
        regionService.saveOrUpdateBatch(enabledRegions)
    }

    /**
     * 启用数据库中已经禁用的，但是当前版本中又被重新启用的信息
     */
    fun reEnabled(allCountyCodes: Set<String>, createDate: LocalDate) {
        val disabledRegions = regionService.findByCountyCodeInAndIsEnabled(allCountyCodes, false)
        logger.debug("在{}被有{}条以前被删除的信息，但是现在重新存在的", createDate, disabledRegions.size)
        disabledRegions.forEach {
            val mark = "该行政区划在${it.disabledDate}版本不存在，在${createDate}版本又被重新恢复"
            it.enabled = true
            it.log += "preEnabled: $mark\n"
            it.disabledDate = null
            it.enabledDate = createDate
        }
        regionService.saveOrUpdateBatch(disabledRegions)
    }

    /**
     * 解析 Json 文件
     */
    fun parseJsonFile(file: File): List<RegionEntity> {
        // JSON 数据的日期
        val filename = file.nameWithoutExtension.substring(0..9)
        val createdDate = filename.toLocalDate()
        // 解析 JSON 数据
        val maps = objectMapper.readValue<ArrayList<Map<String, String>>>(file)
        return maps.map {
            val code = it["code"] ?: error("错误的json数据")
            val name = it["name"] ?: error("错误的json数据")
            RegionEntity(
                // 省级代码
                provinceCode = code.substring(0, 2),
                // 市级代码
                cityCode = code.substring(0, 4),
                // 县级代码
                countyCode = code.substring(0, 6),
                // 乡镇代码
                townCode = "${code.substring(0, 6)}000",
                // 街道代码
                streetCode = "${code.substring(0, 6)}000000",
                // 街道类型
                streetType = "",
                // 街道名称
                streetName = name,
                // 数据级别：省市区乡镇街道
                levelType = LevelType.getByStreetCode("${code.substring(0, 6)}000000"),
                // 是否启用
                enabled = true,
                // 激活日期
                enabledDate = createdDate,
                // 创建日期，第一次出现的日期
                createdDate = createdDate,
                // 类型
                regionType = RegionType.GB2260
            )
        }
    }
}
