package com.china.region.service

import com.baomidou.mybatisplus.extension.service.IService
import com.china.region.entity.RegionEntity
import com.china.region.enums.RegionType
import java.time.LocalDate
import java.time.LocalDateTime

/**
 * @author HouKunLin
 * @date 2020/8/29 0029 19:04
 */
interface RegionService : IService<RegionEntity> {
    fun saveEntity(regionEntity: RegionEntity?, nextMoreInfo: () -> Unit)
    fun findByCountyCode(countyCode: String): RegionEntity?
    fun findByStreetCode(streetCode: String): RegionEntity?
    fun findByCountyCodeNotInAndIsEnabled(countyCodes: Set<String>, isEnabled: Boolean): List<RegionEntity>
    fun findByCountyCodeInAndIsEnabled(countyCodes: Set<String>, isEnabled: Boolean): List<RegionEntity>
    fun countByCountyCodeInAndValidDateGreaterThanEqual(countyCodes: Set<String>, date: LocalDate): Long
    fun findByProvinceCodeAndCountyNameContainingAndIsEnabled(provinceCode: String, countyName: String, isEnabled: Boolean): List<RegionEntity>
    fun setAllValidDate(validDate: LocalDateTime)
    fun setAllValidDate(validDate: LocalDateTime, regionType: RegionType)
    fun finish(list: List<RegionEntity>)
}
