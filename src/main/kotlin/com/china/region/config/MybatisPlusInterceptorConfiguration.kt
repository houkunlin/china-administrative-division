package com.china.region.config

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor
import com.baomidou.mybatisplus.extension.plugins.inner.*
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.transaction.annotation.EnableTransactionManagement
import java.util.function.Consumer

/**
 * 自动配置使用 @PageableDefault 注解来注解 IPage/Page 类型的参数并填充相应的参数信息：当前页码、每页条数、排序字段
 *
 * @author HouKunLin
 */
@Configuration(proxyBeanMethods = false)
@EnableTransactionManagement
@ConditionalOnClass(value = [MybatisPlusInterceptor::class])
class MybatisPlusInterceptorConfiguration(@Value("\${spring.data.web.max-page-size:2000}") maxPageSize: Long) {
    private var maxPageSize: Long = 200

    /**
     * 构造方法
     *
     * @param handlers    数据权限处理器列表
     * @param maxPageSize 分页每页最大数量
     * @see SpringDataWebProperties.Pageable.maxPageSize
     */
    init {
        this.maxPageSize = maxPageSize
    }

    /**
     * 分页插件
     */
    @Order(Ordered.HIGHEST_PRECEDENCE + 2)
    @ConditionalOnMissingBean
    @Bean
    fun paginationInterceptor(): PaginationInnerInterceptor {
        val interceptor = PaginationInnerInterceptor()
        interceptor.maxLimit = maxPageSize
        return interceptor
    }

    /**
     * 乐观锁配置
     */
    @ConditionalOnMissingBean
    @Bean
    fun optimisticLockerInterceptor(): OptimisticLockerInnerInterceptor {
        return OptimisticLockerInnerInterceptor()
    }
//
//    /**
//     * 阻止全表删除: SQL 执行分析拦截器 stopProceed 发现全表执行 delete update 是否停止运行
//     */
//    @ConditionalOnMissingBean
//    @Bean
//    fun blockAttackInnerInterceptor(): BlockAttackInnerInterceptor {
//        return BlockAttackInnerInterceptor()
//    }

    /**
     * MyBatis Plus 拦截器配置 MybatisPlusInterceptor
     */
    @Bean
    fun mybatisPlusInterceptor(interceptors: List<InnerInterceptor?>): MybatisPlusInterceptor {
        val interceptor = MybatisPlusInterceptor()
        interceptors.forEach(Consumer { innerInterceptor: InnerInterceptor? ->
            interceptor.addInnerInterceptor(
                innerInterceptor
            )
        })
        return interceptor
    }
}
