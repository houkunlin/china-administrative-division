package com.china.region.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.china.region.entity.RegionEntity;
import com.china.region.enums.RegionType;
import com.china.region.repository.RegionRepository;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author HouKunLin
 */
@Transactional
@Service
public class RegionServiceImpl extends ServiceImpl<RegionRepository, RegionEntity> implements RegionService {
    private static final Logger logger = LoggerFactory.getLogger(RegionServiceImpl.class);

    @Override
    public void saveEntity(@Nullable RegionEntity regionEntity, @NotNull Function0<Unit> nextMoreInfo) {
        if (regionEntity != null) {
            RegionEntity find = lambdaQuery().eq(RegionEntity::getStreetCode, regionEntity.getStreetCode()).eq(RegionEntity::getEnabledDate, regionEntity.getEnabledDate()).one();
            if (find != null) {
                if (!find.getChildFinish()) {
                    nextMoreInfo.invoke();
                    lambdaUpdate().set(RegionEntity::getChildFinish, true).eq(RegionEntity::getId, find.getId()).update();
                } else {
                    logger.debug("跳过区域：{} {}", regionEntity.getStreetCode(), regionEntity.getStreetName());
                }
            } else {
                save(regionEntity);
                nextMoreInfo.invoke();
                lambdaUpdate().set(RegionEntity::getChildFinish, true).eq(RegionEntity::getId, regionEntity.getId()).update();
            }
        } else {
            nextMoreInfo.invoke();
        }
    }

    @Nullable
    @Override
    public RegionEntity findByCountyCode(@NotNull String countyCode) {
        return lambdaQuery().eq(RegionEntity::getCountyCode, countyCode).one();
    }

    @Nullable
    @Override
    public RegionEntity findByStreetCode(@NotNull String streetCode) {
        return lambdaQuery().eq(RegionEntity::getStreetCode, streetCode).one();
    }

    @NotNull
    @Override
    public List<RegionEntity> findByCountyCodeNotInAndIsEnabled(@NotNull Set<String> countyCodes, boolean isEnabled) {
        return lambdaQuery()
                .notIn(RegionEntity::getCountyCode, countyCodes)
                .eq(RegionEntity::getEnabled, isEnabled)
                .eq(RegionEntity::getRegionType, RegionType.GB2260)
                .list();
    }

    @NotNull
    @Override
    public List<RegionEntity> findByCountyCodeInAndIsEnabled(@NotNull Set<String> countyCodes, boolean isEnabled) {
        return lambdaQuery()
                .in(RegionEntity::getCountyCode, countyCodes)
                .eq(RegionEntity::getEnabled, isEnabled)
                .eq(RegionEntity::getRegionType, RegionType.GB2260)
                .list();
    }

    @Override
    public long countByCountyCodeInAndValidDateGreaterThanEqual(@NotNull Set<String> countyCodes, @NotNull LocalDate date) {
        return lambdaQuery()
                .in(RegionEntity::getCountyCode, countyCodes)
                .le(RegionEntity::getCreatedDate, date)
                .eq(RegionEntity::getRegionType, RegionType.GB2260)
                .count();
    }

    @NotNull
    @Override
    public List<RegionEntity> findByProvinceCodeAndCountyNameContainingAndIsEnabled(@NotNull String provinceCode, @NotNull String countyName, boolean isEnabled) {
        return lambdaQuery()
                .eq(RegionEntity::getProvinceCode, provinceCode)
                .like(RegionEntity::getStreetName, countyName)
                .eq(RegionEntity::getEnabled, isEnabled)
                .eq(RegionEntity::getRegionType, RegionType.GB2260)
                .list();
    }

    @Override
    public void setAllValidDate(@NotNull LocalDateTime validDate) {
        lambdaUpdate().set(RegionEntity::getVerifyDate, validDate).update();
    }

    @Override
    public void setAllValidDate(@NotNull LocalDateTime validDate, @NotNull RegionType regionType) {
        lambdaUpdate().set(RegionEntity::getVerifyDate, validDate).eq(RegionEntity::getRegionType, regionType).update();
    }

    @Override
    public void finish(@NotNull List<RegionEntity> list) {
        Set<Long> ids = list.stream().map(RegionEntity::getId).collect(Collectors.toSet());
        if (ids.isEmpty()) {
            return;
        }
        lambdaUpdate()
                .in(RegionEntity::getId, ids)
                .set(RegionEntity::getChildFinish, true)
                .set(RegionEntity::getVerifyDate, LocalDateTime.now())
                .update();
    }
}
