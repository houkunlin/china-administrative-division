package com.china.region.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.china.region.entity.RegionEntity;
import org.springframework.stereotype.Repository;

/**
 * @author HouKunLin
 */
@Repository
public interface RegionRepository extends BaseMapper<RegionEntity> {
}
