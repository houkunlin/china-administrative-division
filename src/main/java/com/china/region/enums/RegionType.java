package com.china.region.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

public enum RegionType implements IEnum<Integer> {
    /**
     * 中华人民共和国行政区划代码
     */
    GB2260(1, "中华人民共和国行政区划代码"),
    /**
     * 中华人民共和国行政区划代码(县级以下)
     */
    GB10114_88(2, "县以下行政区划代码编制规则"),
    /**
     * 国际标准代码(国家或地区的主要子行政区)
     */
    ISO3166_2(3, "国际标准代码(国家或地区的主要子行政区)"),
    /**
     * 国际标准代码(中国香港)
     */
    ISO3166_2_CN_HK(4, "国际标准代码(中国香港)"),
    /**
     * 国际标准代码(中国澳门)
     */
    ISO3166_2_CN_MO(5, "国际标准代码(中国澳门)"),
    /**
     * 国际标准代码(中国台湾)
     */
    ISO3166_2_CN_TW(6, "国际标准代码(中国台湾)");

    private final Integer value;
    private final String text;

    RegionType(Integer value, String text) {

        this.value = value;
        this.text = text;
    }

    @JsonCreator
    public static RegionType valueOf(Integer value) {
        for (RegionType type : RegionType.values()) {
            if (type.getValue().equals(value)) {
                return type;
            }
        }
        return null;
    }

    public Integer getValue() {
        return value;
    }

    public String getText() {
        return text;
    }
}
