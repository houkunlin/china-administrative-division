package com.china.region.enums;

import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import org.springframework.util.StringUtils;

public enum LevelType implements IEnum<Integer> {
    /**
     * 省
     */
    PROVINCE(1, "省"),
    /**
     * 市
     */
    CITY(2, "市"),
    /**
     * 区（县）
     */
    COUNTY(3, "区（县）"),
    /**
     * 街道（乡镇）
     */
    TOWN(4, "街道（乡镇）"),
    /**
     * 居委会（村委会）
     */
    STREET(5, "居委会（村委会）");

    private final Integer value;
    private final String text;

    LevelType(Integer value, String text) {
        this.value = value;
        this.text = text;
    }

    @JsonCreator
    public static LevelType getByValue(Integer value) {
        for (LevelType levelType : LevelType.values()) {
            if (levelType.getValue().equals(value)) {
                return levelType;
            }
        }
        return null;
    }

    public static LevelType getByStreetCode(String code) {
        if (!StringUtils.hasText(code)) {
            return null;
        }
        if (code.length() == 12) {
            if (code.endsWith("0000000000")) {
                return PROVINCE;
            } else if (code.endsWith("00000000")) {
                return CITY;
            } else if (code.endsWith("000000")) {
                return COUNTY;
            } else if (code.endsWith("000")) {
                return TOWN;
            }
            return STREET;
        }
        return null;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    public String getText() {
        return text;
    }
}
