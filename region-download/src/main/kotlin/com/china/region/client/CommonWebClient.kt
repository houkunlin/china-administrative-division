package com.china.region.client

interface CommonWebClient {
    /**
     * 获取网页内容
     *
     * @param url
     * @return
     */
    fun getHtmlContent(url: String, headers: Map<String, String> = emptyMap()): String;
}
