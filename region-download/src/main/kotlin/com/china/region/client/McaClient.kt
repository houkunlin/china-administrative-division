package com.china.region.client

/**
 * 民政部接口
 *
 * @author HouKunLin
 */
interface McaClient {
    /**
     * 民政部新版网站的内容
     *
     * @return 网页内容
     */
    fun getGb2260YearPage0(): String

    /**
     * 民政部新版网站的历史数据
     *
     * @return 网页内容
     */
    fun getGb2260YearPage1(): String

    /**
     * 民政部新版网站的历史数据
     *
     * @return 网页内容
     */
    fun getGb2260YearPage2(): String

    /**
     * 民政部新版网站的历史数据
     *
     * @return 网页内容
     */
    fun getGb2260YearPage3(): String

    companion object {
        const val HOST = "https://www.mca.gov.cn"
    }
}
