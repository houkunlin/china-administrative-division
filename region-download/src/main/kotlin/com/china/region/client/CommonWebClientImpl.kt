package com.china.region.client

import com.china.region.util.toStringAutoEncoding
import org.springframework.http.codec.ClientCodecConfigurer
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.ExchangeStrategies
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono

@Component
class CommonWebClientImpl : CommonWebClient {
    // 配置默认缓冲区大小，否则默认 256K
    val strategies = ExchangeStrategies.builder()
        .codecs { codecs: ClientCodecConfigurer -> codecs.defaultCodecs().maxInMemorySize(-1) }
        .build()

    override fun getHtmlContent(url: String, headers: Map<String, String>): String {
        val client = WebClient.builder()
            .exchangeStrategies(strategies)
            .baseUrl(url)
            .build()
        val webClient = client.get()
        headers.forEach { (k: String, v: String?) ->
            webClient.header(k, v)
        }
        val block = webClient.retrieve().bodyToMono<ByteArray>().block() ?: return ""

        return block.toStringAutoEncoding()
    }
}
