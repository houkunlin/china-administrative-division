package com.china.region.client

import org.springframework.stereotype.Component

@Component
class McaClientImpl(val commonWebClient: CommonWebClient) : McaClient {
    override fun getGb2260YearPage0(): String {
        return commonWebClient.getHtmlContent("${McaClient.HOST}/n156/n2679/index.html")
    }

    override fun getGb2260YearPage1(): String {
        return commonWebClient.getHtmlContent("${McaClient.HOST}/n156/n186/index.html")
    }

    override fun getGb2260YearPage2(): String {
        return commonWebClient.getHtmlContent("${McaClient.HOST}/n156/n186/index_1284_2.html")
    }

    override fun getGb2260YearPage3(): String {
        return commonWebClient.getHtmlContent("${McaClient.HOST}/n156/n186/index_1284_1.html")
    }
}
