package com.china.region.gb2260

import com.alibaba.fastjson.JSONObject
import com.alibaba.fastjson.serializer.SerializerFeature
import com.china.region.client.CommonWebClient
import com.china.region.client.McaClient
import org.apache.commons.codec.digest.DigestUtils
import org.joda.time.DateTime
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import java.io.File
import java.net.URL
import java.util.*

/**
 * 民政部行政区划代码获取。
 * 从民政部获取各个年份的行政区划代码到本地保存到JSON文件里面
 *
 * @author HouKunLin
 * @date 2020/8/31 0031 10:30
 */
@Order(100)
@Component
class MacDataHandler(val client: McaClient, val commonWebClient: CommonWebClient) : CommandLineRunner {
    override fun run(vararg args: String?) {
        val parent = File("data/GB2260/")
        if (!parent.exists()) {
            parent.mkdirs()
        }
        getYearRegionUrls { key, _, url ->
            if (hasKeyFiles(parent, key)) {
                // 已经存在该日期的文件
                logger.info("年份：{} ，已经存在该日期的文件，跳过", key)
                return@getYearRegionUrls
            }
            val entities = parseRegion(key, url)
            val list = entities.map { mapOf(Pair("code", it.countyCode), Pair("name", it.countyName)) }
            logger.info("年份：{} ，实体类条数：{} ，Map 条数：{}", key, entities.size, list.size)
            val json = JSONObject.toJSONString(list, SerializerFeature.PrettyFormat)
            val hash = DigestUtils.sha1Hex(json).uppercase(Locale.getDefault())

            val file = File(parent, "${key}-${list.size}-${hash}.json")
            file.writeText(json)
        }
    }

    fun hasKeyFiles(parent: File, key: String): Boolean {
        val findFiles = parent.listFiles()?.filter { it.extension == "json" && it.nameWithoutExtension.startsWith(key) }
            ?: emptyList()
        return findFiles.isNotEmpty()
    }

    /**
     * 获取所有区划代码年份URL地址列表
     */
    fun getYearRegionUrls(action: (key: String, title: String, url: URL) -> Unit) {
        val html0 = client.getGb2260YearPage0()
        val html1 = client.getGb2260YearPage1()
        val html2 = client.getGb2260YearPage2()
        val html3 = client.getGb2260YearPage3()
        val document0 = Jsoup.parse(html0)
        val document1 = Jsoup.parse(html1)
        val document2 = Jsoup.parse(html2)
        val document3 = Jsoup.parse(html3)
        val articles0 = document0.select(".article .arlisttd a")
        val articles1 = document1.select(".article .artitlelist")
        val articles2 = document2.select(".article .artitlelist")
        val articles3 = document3.select(".article .artitlelist")

        val elements = ArrayList<Element>()

        elements.addAll(articles0)
        elements.addAll(articles1)
        elements.addAll(articles2)
        elements.addAll(articles3)

        if (elements.isEmpty()) {
            return
        }
        elements.reverse()

        val context = URL("${McaClient.HOST}/n156/n186/index.html")

        elements.forEachIndexed { _, element ->
            val attrTitle = element.attr("title")
            val uri = element.attr("href")
            val url = URL(context, uri)
            if (attrTitle.contains(Regex("^([0-9]{4})年中华人民共和国行政区划代码.*(截止|截至).*"))) {
                val key = getYearDateByTitle1(attrTitle)
                println("$key  $attrTitle  $url")
                action.invoke(key, attrTitle, url)
            } else if (attrTitle.contains(Regex("^([0-9]{4})年中华人民共和国行政区划代码$"))) {
                val html = commonWebClient.getHtmlContent(url.toString())
                val document = Jsoup.parse(html)
                val aList = document.select(".artext #zoom.content a")
                aList.forEach { aElem ->
                    val title = aElem.text()
                    val key = getYearDateByTitle2(title)
                    val newUrl = URL(url, aElem.attr("href"))
                    if (key.isNotBlank()) {
                        println("$key  $title  $newUrl")
                        action.invoke(key, title, newUrl)
                    }
                }
            } else if (attrTitle.contains(Regex("^([0-9]{4})年中华人民共和国县以上行政区划代码$"))) {
                val key =
                    Regex("^([0-9]{4})年中华人民共和国县以上行政区划代码$").matchEntire(attrTitle)?.groupValues?.get(1)
                        ?: ""
                println("$key  $attrTitle  $url")
                action.invoke("$key-12-31", attrTitle, url)
            } else {
                return@forEachIndexed
            }
        }
    }

    /**
     * 通过标题获取截止年份日期信息
     */
    fun getYearDateByTitle1(title: String): String {
        val regex = Regex(".*(截止|截至)([0-9]{4})年([0-9]{2})月([0-9]{2})日.*")
        val group = arrayListOf<String>()
        group.addAll(regex.matchEntire(title)?.groupValues ?: emptyList())
        if (group.isEmpty()) {
            return ""
        }
        // 移除前两个元素
        group.removeAt(0)
        group.removeAt(0)

        return group.joinToString("-")
    }

    /**
     * 通过标题获取截止年份日期信息
     */
    fun getYearDateByTitle2(title: String): String {
        val regex = Regex(".*年中华人民共和国县以上行政区划代码.*(截止|截至)([0-9]{4})年([0-9]{2})月([0-9]{2})日.*")
        val group = arrayListOf<String>()
        group.addAll(regex.matchEntire(title)?.groupValues ?: emptyList())
        if (group.isEmpty()) {
            return ""
        }
        // 移除前两个元素
        group.removeAt(0)
        group.removeAt(0)

        return group.joinToString("-")
    }

    /**
     * 从网页中解析区划信息
     * @param year 年份
     * @param url 该年份的区划网页地址
     */
    fun parseRegion(year: String, url: URL): List<RegionEntity> {
        val list = arrayListOf<RegionEntity>()
        println()
        val html = commonWebClient.getHtmlContent(url.toString())
        val jsoup = Jsoup.parse(html)
        val aList = jsoup.select(".artext #zoom.content a")
        if (aList.isNotEmpty()) {
            aList.forEach { aElem ->
                val title = aElem.text()
                val key = getYearDateByTitle2(title)
                if (key.isNotBlank()) {
                    val newUrl = URL(url, aElem.attr("href"))
                    logger.debug("Href Value: {}", newUrl)

                    list.addAll(parseRegion(year, newUrl))
                }
            }
        } else {
            val trs = jsoup.select("table tr")
            val date = DateTime(year).toDate()
            trs.forEachIndexed { index, element ->
                var text = element.text()
                text = text.replace(" ", "")
                if (text.isBlank() || text.length <= 6 || !text.contains(Regex("^[0-9]{6}"))) {
                    return@forEachIndexed
                }
                try {
                    val entity = RegionEntity(
                        provinceCode = text.substring(0, 2),
                        cityCode = text.substring(0, 4),
                        countyCode = text.substring(0, 6),
                        countyName = text.substring(6),
                        enabledDate = date,
                        createdDate = date
                    )
                    list.add(entity)
                } catch (e: Exception) {
                    logger.debug("index: {}, value: {}", index, text)
                    logger.debug("html: {}", element.html())
                    logger.error("截取字符串错误", e)
                }
            }
        }
        return list
    }

    companion object {
        private val logger = LoggerFactory.getLogger(MacDataHandler::class.java)!!
    }
}

data class RegionEntity(
    /**
     * 省、直辖市、自治区代码
     */
    val provinceCode: String,
    /**
     * 城市代码
     */
    val cityCode: String,
    /**
     * 区县代码
     */
    val countyCode: String,
    /**
     * 区县名称
     */
    val countyName: String,
    /**
     * 区划激活时间
     */
    val enabledDate: Date,
    /**
     * 第一次出现的年份
     */
    val createdDate: Date,
)
