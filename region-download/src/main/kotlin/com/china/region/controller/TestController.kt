package com.china.region.controller

import com.houkunlin.system.common.ErrorMessage
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*
import kotlin.collections.HashMap

/**
 * @author HouKunLin
 */
@RestController
@RequestMapping("/test")
class TestController {
    private val logger = LoggerFactory.getLogger(TestController::class.java)!!

    /**
     * 服务器本地时区信息
     *
     * @return JsonResult JSON数据
     */
    @GetMapping("timeZone")
    fun timeZone(): Any {
        val timeZone = TimeZone.getDefault()
        val json = HashMap<String, Any>()

        json.put("ID", timeZone.id)
        json.put("displayName", timeZone.displayName)
        json.put("zoneId", timeZone.toZoneId())
        json.put("DSTSavings", timeZone.dstSavings)
        json.put("useDaylightTime", timeZone.useDaylightTime())
        json.put("observesDaylightTime", timeZone.observesDaylightTime())
        return ErrorMessage.ok().setData(json)
    }
}
