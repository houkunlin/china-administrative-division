package com.china.region.util

import org.mozilla.universalchardet.UniversalDetector
import java.nio.charset.Charset

/**
 * 自动识别编码并转换成字符串
 *
 * @return 字符串
 */
fun ByteArray.toStringAutoEncoding(): String {
    return String(this, Charset.forName(getEncoding(this)))
}

private fun getEncoding(bytes: ByteArray): String {
    val detector = UniversalDetector(null)
    detector.handleData(bytes, 0, bytes.size)
    detector.dataEnd()
    val encoding = detector.detectedCharset
    detector.reset()
    return encoding ?: "UTF-8"
}
