package com.china.region

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableAsync

@SpringBootApplication(scanBasePackages = ["com.china.region", "com.houkunlin.system.common"])
@EnableAsync
class RegionApplication

fun main(args: Array<String>) {
    runApplication<RegionApplication>(*args)
}
