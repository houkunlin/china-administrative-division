package com.china.region.gb10114

import com.china.region.client.CommonWebClient
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.joda.time.DateTime
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import java.io.File
import java.net.URL
import kotlin.math.pow

/**
 * 统计局县级以下行政区划代码获取。
 * 从统计局获取各个年份的县级行政区划代码到本地保存到html文件里面
 * @author HouKunLin
 * @date 2020/8/31 0031 10:48
 */
@Order(200)
@Component
class StatsDataHandler(val commonWebClient: CommonWebClient) : CommandLineRunner {
    companion object {
        private val logger = LoggerFactory.getLogger(StatsDataHandler::class.java)!!
        val beforeYearTitle = Regex(".*截止([0-9]{4})年([0-9]{1,2})月([0-9]{1,2})日.*")
        val beforeYearTitle1 = Regex(".*([0-9]{4})年([0-9]{1,2})月([0-9]{1,2})日.*")
        const val HOST = "http://www.stats.gov.cn"
    }

    override fun run(vararg args: String?) {
        doYearDataEntry(
            arrayOf(
                2009,
                2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019,
                2020, 2021, 2022, 2023
            )
        ) { date: String, url: URL ->
            waitRun {
                doProvinceDataEntry(date, url)
            }
        }
    }

    /**
     * 获取各个年份首页信息，涵盖各省份数据入口地址
     *
     * @param action
     * @receiver
     */
    private fun doYearDataEntry(years: Array<Int>, action: (date: String, url: URL) -> Unit) {
        for (year in years) {
            val url = URL("${HOST}/sj/tjbz/tjyqhdmhcxhfdm/$year/index.html")
            val html = commonWebClient.getHtmlContent(url.toString())
            val document = Jsoup.parse(html)
            val htmlText = document.text()
            val regex = Regex(".*([0-9]{4})年([0-9]{1,2})月([0-9]{1,2})日.*")
            val group = arrayListOf<String>()
            group.addAll(regex.matchEntire(htmlText)?.groupValues ?: emptyList())
            val key = if (group.isEmpty()) {
                "$year-12-31"
            } else {
                group.removeAt(0)
                if (group[1].length == 1) {
                    group[1] = "0${group[1]}"
                }
                group.joinToString("-")
            }
            action.invoke(key, url)
        }
    }

    /**
     * 获取这个年份的各省份数据
     */
    fun doProvinceDataEntry(yearDate: String, url: URL) {
        println()
        if (isFinish(url)) {
            return
        }
        logger.debug("{} GET {}", yearDate, url)
        val html = try {
            commonWebClient.getHtmlContent(url.toString())
        } catch (e: Exception) {
            throw Exception(url.toString(), e)
        }
        checkJavaScriptRefresh(url, html)
        saveFile(url, html)

        val jsoup = Jsoup.parse(html)
        val trs = jsoup.select("table tr.provincetr a")
        val dateString = getTitle(yearDate, jsoup)
        val date = DateTime(dateString).toLocalDate().toString()
        trs.forEachIndexed { _, element ->
            val text = element.text()
            // 获取 yearDate 年份下的 text 省份下的市级名单列表
            waitRun {
                doCityAndMoreData(date, URL(url, element.attr("href")), text)
            }
        }
        if (trs.isNotEmpty()) {
            finish(url, String.format("正在获取 %s 年数据 共 %s 个省份", yearDate, trs.size))
        } else {
            logger.info("正在获取 {} 年数据，获取数据失败，无数据 {}", yearDate, url)
        }
    }

    fun getTitle(defaultTitle: String, jsoup: Document): String {
        val title = jsoup.selectFirst(".provincehead")?.text() ?: ""
        val body = jsoup.selectFirst("body")?.text() ?: ""
        logger.debug(title)
        return if (title.contains(beforeYearTitle)) {
            val arr = arrayListOf<String>()
            arr.addAll(beforeYearTitle.matchEntire(title)?.groupValues ?: arrayListOf())
            println(arr)
            arr.removeAt(0)
            arr.joinToString("-")
        } else if (body.contains(beforeYearTitle1)) {
            val arr = arrayListOf<String>()
            arr.addAll(beforeYearTitle1.matchEntire(body)?.groupValues ?: arrayListOf())
            println(arr)
            arr.removeAt(0)
            arr.joinToString("-")
        } else {
            defaultTitle
        }
    }

    /**
     * 加载市级和市级以下。
     * dept: 1市级列表，2县级列表，3乡级、街道列表，4居委会、村委会
     */
    fun doCityAndMoreData(date: String, url: URL, title: String, dept: Int = 1) {
        if (isFinish(url)) {
            return
        }
        logger.debug("正在获取 {} 年份 {} 的数据 GET {}", date, title, url)
        val html = try {
            commonWebClient.getHtmlContent(url.toString())
        } catch (e: Exception) {
            throw Exception(url.toString(), e)
        }
        checkJavaScriptRefresh(url, html)
        saveFile(url, html)

        val jsoup = Jsoup.parse(html)
        val trs = arrayListOf<Element>()
        jsoup.select(".citytr").forEach { trs.add(it) }
        jsoup.select(".countytr").forEach { trs.add(it) }
        jsoup.select(".towntr").forEach { trs.add(it) }
        jsoup.select(".villagetr").forEach { trs.add(it) }

        logger.debug("已经得到 {} 年份 {} 的数据 总共 {} 条记录", date, title, trs.size)

        if (dept == 3) {
            runBlocking {
                trs.forEach {
                    val text = it.text()?.replace(" ", "") ?: ""
                    val href = it.selectFirst("td a")?.attr("href") ?: ""
                    if (href.isNotBlank()) {
                        val path = URL(url, href)
                        // 获取县级和县级以下
                        launch {
                            waitRun {
                                doCityAndMoreData(date, path, text, dept + 1)
                            }
                        }
                    }
                }
            }
        } else {
            trs.forEach {
                val text = it.text()?.replace(" ", "") ?: ""
                val href = it.selectFirst("td a")?.attr("href") ?: ""
                if (href.isNotBlank()) {
                    val path = URL(url, href)
                    // 获取县级和县级以下
                    waitRun {
                        doCityAndMoreData(date, path, text, dept + 1)
                    }
                }
            }
        }

        if (trs.isNotEmpty()) {
            finish(url, String.format("已经得到 %s 年份 %s 的数据 总共 %s 条记录", date, title, trs.size))
        } else {
            logger.error("正在获取 {} 年份 {} 的数据，获取数据失败，无数据 {}", date, title, url)
        }
    }

    fun waitRun(action: () -> Unit) {
        var num = 1
        while (true) {
            try {
                sleep(500)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            try {
                action()
                break
            } catch (e: Exception) {
                e.printStackTrace()
                val time = if (++num > 10) {
                    ((2.0.pow(10.0) + num) * 200).toLong()
                } else {
                    (2.0.pow(num * 1.0) * 200).toLong()
                }
                println(String.format("正在睡眠 %s 次 %ss，然后进行重试", num, time))
                sleep(time)
            }
        }
    }

    fun checkJavaScriptRefresh(url: URL, text: String) {
        if (text.contains("请开启JavaScript并刷新该页")) {
            sleep(5 * 1000)
            throw Exception("$url - 请开启JavaScript并刷新该页")
        }
    }

    fun getFile(uri: String): File {
        return if (uri.replace(Regex("/+"), "/").startsWith("/")) {
            File("data/", uri.substring(1))
        } else {
            File("data/", uri)
        }
    }

    fun saveFile(url: URL, html: String) {
        val file = getFile(getUrlLocalFilePath(url))
        val parent = file.parentFile
        if (!parent.exists()) {
            parent.mkdirs()
        }
        file.writeText(html)
    }

    fun isFinish(url: URL): Boolean {
        val file = getFile(getUrlLocalFilePath(url).replace(Regex("\\.(html|htm)"), ".finish"))
        if (file.nameWithoutExtension == "350527") {
            // 福建省泉州市金门县出现404
            return true
        }
        return file.exists()
    }

    fun finish(url: URL, text: String) {
        val file = getFile(getUrlLocalFilePath(url).replace(Regex("\\.(html|htm)"), ".finish"))
        file.writeText(DateTime().toString("yyyy-MM-dd HH:mm:ss") + "\n" + text)
    }

    fun getUrlLocalFilePath(url: URL): String {
        return url.path.replace("/sj/", "/tjsj/")
    }

    fun sleep(time: Long) {
        try {
            Thread.sleep(time)
        } catch (e: Exception) {
        }
    }
}
