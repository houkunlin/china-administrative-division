/*
 Navicat Premium Data Transfer

 Source Server         : nas - MySQL (root)
 Source Server Type    : MySQL
 Source Server Version : 80100
 Source Host           : 192.168.0.5:3306
 Source Schema         : gb2260

 Target Server Type    : MySQL
 Target Server Version : 80100
 File Encoding         : 65001

 Date: 20/10/2023 17:17:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for region_2023
-- ----------------------------
DROP TABLE IF EXISTS `region_2023`;
CREATE TABLE `region_2023`  (
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户ID',
  `revision` int NULL DEFAULT 0 COMMENT '乐观锁字段',
  `is_deleted` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `created_by` bigint NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(6) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(6) NULL DEFAULT NULL COMMENT '更新时间',
  `id` bigint NOT NULL COMMENT '主键',
  `province_code` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '省、直辖市、自治区代码',
  `province_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '省、直辖市、自治区名称',
  `city_code` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '城市代码',
  `city_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '城市名称',
  `county_code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '区县代码',
  `county_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '区县名称',
  `town_code` varchar(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '乡镇代码',
  `town_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '乡镇名称',
  `street_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '街道代码',
  `street_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '街道名称',
  `street_type` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '城乡街道分类代码',
  `level_type` int NULL DEFAULT NULL COMMENT '数据级别：省市区县乡镇街道',
  `enabled_date` date NULL DEFAULT NULL COMMENT '区划激活时间',
  `created_date` date NULL DEFAULT NULL COMMENT '第一次出现的年份',
  `disabled_date` date NULL DEFAULT NULL COMMENT '某个区域被取消的时候会在这里标记年份',
  `disabled_mark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL COMMENT '取消的原因',
  `log` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL COMMENT '变更日志',
  `is_enabled` bit(1) NULL DEFAULT NULL COMMENT '由于历史变更，有些乡变镇，县变区，县变市的城市会从历史中消失不见，此时将该记录给隐藏',
  `verify_date` datetime(6) NULL DEFAULT NULL COMMENT '信息验证时间，最新检查时间',
  `region_type` int NULL DEFAULT NULL COMMENT '数据类型：1:GB2260、2:GB10114_88',
  `child_finish` bit(1) NULL DEFAULT NULL COMMENT '是否处理完子类的信息',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `search`(`street_code` ASC, `street_name` ASC, `enabled_date` ASC) USING BTREE,
  INDEX `time`(`enabled_date` ASC, `created_date` ASC, `disabled_date` ASC, `verify_date` ASC, `created_time` ASC, `updated_time` ASC) USING BTREE,
  INDEX `search_code`(`province_code` ASC, `city_code` ASC, `county_code` ASC) USING BTREE,
  INDEX `search_name`(`province_name` ASC, `city_name` ASC, `county_name` ASC) USING BTREE,
  INDEX `key1`(`tenant_id` ASC) USING BTREE,
  INDEX `key2`(`is_deleted` ASC) USING BTREE,
  INDEX `key3`(`province_code` ASC) USING BTREE,
  INDEX `key4`(`province_name` ASC) USING BTREE,
  INDEX `key5`(`city_code` ASC) USING BTREE,
  INDEX `key6`(`city_name` ASC) USING BTREE,
  INDEX `key7`(`county_code` ASC) USING BTREE,
  INDEX `key8`(`county_name` ASC) USING BTREE,
  INDEX `key9`(`town_code` ASC, `town_name` ASC) USING BTREE,
  INDEX `key10`(`street_code` ASC, `street_name` ASC) USING BTREE,
  INDEX `key11`(`enabled_date` ASC, `created_date` ASC, `disabled_date` ASC, `is_enabled` ASC, `verify_date` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '行政区划代码' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for region_2023_simple
-- ----------------------------
DROP TABLE IF EXISTS `region_2023_simple`;
CREATE TABLE `region_2023_simple`  (
  `tenant_id` bigint NULL DEFAULT NULL COMMENT '租户ID',
  `revision` int NULL DEFAULT 0 COMMENT '乐观锁字段',
  `is_deleted` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `created_by` bigint NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime(6) NULL DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint NULL DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime(6) NULL DEFAULT NULL COMMENT '更新时间',
  `id` bigint NOT NULL COMMENT '主键',
  `province_code` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '省、直辖市、自治区代码',
  `city_code` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '城市代码',
  `county_code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '区县代码',
  `town_code` varchar(9) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '乡镇代码',
  `street_code` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '街道代码',
  `street_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '街道名称',
  `street_type` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL DEFAULT NULL COMMENT '城乡街道分类代码',
  `level_type` int NULL DEFAULT NULL COMMENT '数据级别：省市区县乡镇街道',
  `enabled_date` date NULL DEFAULT NULL COMMENT '区划激活时间',
  `created_date` date NULL DEFAULT NULL COMMENT '第一次出现的年份',
  `disabled_date` date NULL DEFAULT NULL COMMENT '某个区域被取消的时候会在这里标记年份',
  `disabled_mark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL COMMENT '取消的原因',
  `log` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_as_cs NULL COMMENT '变更日志',
  `is_enabled` bit(1) NULL DEFAULT NULL COMMENT '由于历史变更，有些乡变镇，县变区，县变市的城市会从历史中消失不见，此时将该记录给隐藏',
  `verify_date` datetime(6) NULL DEFAULT NULL COMMENT '信息验证时间，最新检查时间',
  `region_type` int NULL DEFAULT NULL COMMENT '数据类型：1:GB2260、2:GB10114_88',
  `child_finish` bit(1) NULL DEFAULT NULL COMMENT '是否处理完子类的信息',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `key1`(`tenant_id` ASC) USING BTREE,
  INDEX `key2`(`is_deleted` ASC) USING BTREE,
  INDEX `key3`(`province_code` ASC) USING BTREE,
  INDEX `key4`(`city_code` ASC) USING BTREE,
  INDEX `key5`(`county_code` ASC) USING BTREE,
  INDEX `key6`(`town_code` ASC) USING BTREE,
  INDEX `key7`(`street_code` ASC) USING BTREE,
  INDEX `key8`(`street_name` ASC) USING BTREE,
  INDEX `key9`(`level_type` ASC, `region_type` ASC) USING BTREE,
  INDEX `key10`(`is_enabled` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_as_cs COMMENT = '行政区划代码' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
