# 数据存放路径

<!-- TOC -->
* [数据存放路径](#数据存放路径)
  * [目录文件说明](#目录文件说明)
    * [目录：`GB2260/`](#目录gb2260)
    * [目录：`GB10114-88/`](#目录gb10114-88)
    * [文件：`region_2020_struct.sql` 和 `region_2020_struct_2020_data.sql.7z`](#文件region2020structsql-和-region2020struct2020datasql7z)
    * [文件：`region_2023_struct_2022_data.sql.7z`](#文件region2023struct2022datasql7z)
    * [文件：`region_2023_struct_2023_data.sql.7z`](#文件region2023struct2023datasql7z)
    * [文件：`tjsj.7z` 文件数据已过时，最新数据请参考 GB10114-88 文件夹内数据](#文件tjsj7z-文件数据已过时最新数据请参考-gb10114-88-文件夹内数据)
<!-- TOC -->

## 目录文件说明

### 目录：`GB2260/`

民政局从 `1980` 年到 `2023` 年的所有行政区划数据json文件.

其中 [GB2260/2023-12-31-3209-474D5E4AA1EEE7555B39E6A90484164E1809D901-Fixup.json](GB2260/2023-12-31-3209-474D5E4AA1EEE7555B39E6A90484164E1809D901-Fixup.json) 是对
[GB2260/2023-12-31-3209-474D5E4AA1EEE7555B39E6A90484164E1809D901.json](GB2260/2023-12-31-3209-474D5E4AA1EEE7555B39E6A90484164E1809D901.json) 的内容修正。

根据官方说明，原始文件中 **标记“*”的行政区划代码第三、四位90表示省（自治区）直辖县级行政区划汇总码** ，修正后的文件内容将删除“*”号内容。

### 目录：`GB10114-88/`

在 2020 年获取的统计局公布的县以下（GB10114-88）区划网页数据

### 文件：`region_2020_struct.sql` 和 `region_2020_struct_2020_data.sql.7z`

由之前的 `region-GB2260.sql` 、 `region-GB2260-GB10114_88-半成品.sql` 、 `region-GB2260-GB10114_88-完整版.sql` 打包压缩而来。

数据截止日期：`2020-09-16` （数据更新截止日期）

解压后：

- `region-GB2260.sql` 解析民政局从1980年到2020年的所有行政区划数据json文件得到的数据库数据，涵盖数据历史变更
- `region-GB2260-GB10114_88-半成品.sql` 涵盖region-GB2260.sql和统计局2019年县级以下的数据，未处理（填充、补充）单条数据的省、市、区县、乡镇名称
- `region-GB2260-GB10114_88-完整版.sql` 涵盖region-GB2260.sql和统计局2019年县级以下的数据，已处理（填充、补充）单条数据的省、市、区县、乡镇名称

文件摘要：
- `region_2020_struct_2020_data.sql.7z` MD5: `18A322785B61FEB193957469F08B7C79`
- `region_2020_struct_2020_data.sql.7z` SHA1: `FAE1453905E3FD9070607A3CDF8D308A3C2A4064`
- `region_2020_struct_2020_data.sql.7z` SHA256: `6F89F617E7C906B90144AB538EC5207D802D753733DF0539D4902694C28A2346`
- `region-GB2260.sql` MD5: `3FF27B9B818572A6581C1A9F76A789E2`
- `region-GB2260.sql` SHA1: `26CD0149CF62E22A49587B3D85D907D4939D9D3A`
- `region-GB2260.sql` SHA256: `12E1596355D7535B399942DB79C3AB0E019156113836D21490F5661A3329AA3D`
- `region-GB2260-GB10114_88-半成品.sql` MD5: `D45506C8E00C32FC0606A789B8A3FEEE`
- `region-GB2260-GB10114_88-半成品.sql` SHA1: `69618B1398AB62C03DFC8898DFFF70F4E3605068`
- `region-GB2260-GB10114_88-半成品.sql` SHA256: `6EA0684302A054A2391F2977FB093D9C015E2F7C1551B8BE3DC493AEC06A47A9`
- `region-GB2260-GB10114_88-完整版.sql` MD5: `A69CD7FCFC85B14A3FC05DF43FE9567D`
- `region-GB2260-GB10114_88-完整版.sql` SHA1: `5F08CC86D551E89F35CC01754EBDFD2D2C10DA1B`
- `region-GB2260-GB10114_88-完整版.sql` SHA256: `3C9EB903BFDB0BCAF33C0CF07AC10931F1DC1A1313DFC026676184760B572D83`


### 文件：`region_2023_struct_2022_data.sql.7z`

数据截止日期：`2023-10-20` （数据更新截止日期）

解压后：
- `region_2023_struct_2022_data.sql` 解析民政局从1980年到2022年的所有行政区划数据json文件得到的数据库数据，涵盖数据历史变更；统计局2022年县级以下的数据；全部采用 GB10114-88 县以下 编码格式存储数据；

文件摘要：
- `region_2023_struct_2022_data.sql.7z` MD5: `396C223448F2A117959F20D392EC39BE`
- `region_2023_struct_2022_data.sql.7z` SHA1: `7F7478616AA9E43B3102DC2C4C2DFAAF82AF14EB`
- `region_2023_struct_2022_data.sql.7z` SHA256: `58F5E472E57D4C15679C865372AACEC937C24217452342BF9E9619DF852D4A9E`
- `region_2023_struct_2022_data.sql` MD5: `0DF907E590E1424D7C3E76374E2971F1`
- `region_2023_struct_2022_data.sql` SHA1: `F1DA4C1164D2B2846C3C4F475BD1576D6649B1D4`
- `region_2023_struct_2022_data.sql` SHA256: `1085DF3BA6016F201B94D3AF609978771E31EA66EBE8FA488882E92B5437768A`


### 文件：`region_2023_struct_2023_data.sql.7z`

数据截止日期：`2023-10-21` （数据更新截止日期）

解压后：
- `region_2023_struct_2023_data.sql` 解析民政局从1980年到2022年的所有行政区划数据json文件得到的数据库数据，涵盖数据历史变更；统计局2023年县级以下的数据；全部采用 GB10114-88 县以下 编码格式存储数据；
- `region_2023_struct_2023_data_upgradle.sql` 人工校验纠正数据SQL执行语句，纠正 `[米林县(540422)]变[米林市(540481)]` 和 `[错那县(540530)]变[错那市(540581)]`；**请注意**：`[米林市(540481)]` 和 `[错那市(540581)]` 的 `region_type` 字段值依旧为 `2 - GB10114_88` 类型，未改成 `1 - GB2260` 类型

文件摘要：
- `region_2023_struct_2023_data.sql.7z` MD5: `8C254351DBB0CC208B00CFB6402BB33A`
- `region_2023_struct_2023_data.sql.7z` SHA1: `94650EA948F66CCD25BF12DF7C7EBC431C31791D`
- `region_2023_struct_2023_data.sql.7z` SHA256: `757F0025D77500BD406AFD9CB7E285EC77F41C241F50697A457568FD9FE2D75A`
- `region_2023_struct_2023_data.sql` MD5: `761436C96C8610106EB03BC58DD8A821`
- `region_2023_struct_2023_data.sql` SHA1: `12E7EFE3BA4E58390DA91F556685214D5F7D0DD2`
- `region_2023_struct_2023_data.sql` SHA256: `4835F9F31E87F50E5FFBFD7BA03E174135844C42916EBCD53A736FFAF1138128`
- `region_2023_struct_2023_data_upgradle.sql` MD5: `B4F58886D99F912CE0D1244F809BEB54`
- `region_2023_struct_2023_data_upgradle.sql` SHA1: `295A3F93FB2CE72C34B106C33B27C184D663CFC4`
- `region_2023_struct_2023_data_upgradle.sql` SHA256: `E2C82A57A217B3D6465843E6959709F3C3609F922CDD2664E4A9686030229DBA`

### 文件：`tjsj.7z` 文件数据已过时，最新数据请参考 [GB10114-88](GB10114-88) 文件夹内数据

数据截止日期：`2020-09-14` （数据更新截止日期）

这是在2020年获取到的统计局2009-2019年的所有行政区划数据网页文件，数据量有点大，约有50万个网页文件+50万个网页对应的标记完成文件，总计一百万个文件，解压后约有2G大小。

缺少`福建省泉州市金门县`行政区划为`350527`的数据，抓取数据的时候这个区划的数据出现404错误

文件摘要：
- `tjsj.7z` MD5: `108EBF2ADBDD5535A998ED1E42018FAB`
- `tjsj.7z` SHA1: `B62E8557596C2E6EE97A14A886B21C90E26C800C`
- `tjsj.7z` SHA256: `4AFCF3137451ACB1D2193D905859C0D36769F00D23D73A8CBEC38A19D6E3BFFC`
