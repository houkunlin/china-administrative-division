/*
 Navicat Premium Data Transfer

 Source Server         : Docker-MySQL8.0
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : 192.168.99.101:3306
 Source Schema         : china

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 13/09/2020 00:02:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for region
-- ----------------------------
DROP TABLE IF EXISTS `region`;
CREATE TABLE `region`  (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `province_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '省、直辖市、自治区代码',
  `province_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '省、直辖市、自治区名称',
  `city_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '城市代码',
  `city_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '城市名称',
  `county_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '区县代码',
  `county_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '区县名称',
  `town_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '乡镇代码',
  `town_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '乡镇名称',
  `street_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '街道代码',
  `street_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '街道名称',
  `street_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '城乡街道分类代码',
  `enabled_date` date NULL DEFAULT NULL COMMENT '区划激活时间',
  `created_date` date NULL DEFAULT NULL COMMENT '第一次出现的年份',
  `disabled_date` date NULL DEFAULT NULL COMMENT '某个区域被取消的时候会在这里标记年份',
  `disabled_mark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '取消的原因',
  `log` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '变更日志',
  `enabled` bit(1) NULL DEFAULT NULL COMMENT '由于历史变更，有些乡变镇，县变区，县变市的城市会从历史中消失不见，此时将该记录给隐藏',
  `valid_date` timestamp(6) NULL DEFAULT NULL COMMENT '信息验证时间，最新检查时间',
  `region_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '数据类型',
  `child_finish` bit(1) NULL DEFAULT NULL COMMENT '是否处理完子类的信息',
  `version` int(0) NULL DEFAULT 0 COMMENT '乐观锁字段',
  `gmt_created` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '数据创建时间',
  `gmt_modified` timestamp(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '数据更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `search`(`street_code`, `street_name`, `enabled_date`) USING BTREE,
  INDEX `time`(`enabled_date`, `created_date`, `disabled_date`, `valid_date`, `gmt_created`, `gmt_modified`) USING BTREE,
  INDEX `search_code`(`province_code`, `city_code`, `county_code`) USING BTREE,
  INDEX `search_name`(`province_name`, `city_name`, `county_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;
